import React from 'react'
import ReactDOM from 'react-dom';
import { useStores } from './hooks/use-stores'
import {toJS} from "mobx";
import BootstrapProvider from '@bootstrap-styled/provider';
import {
    Container,
    Row,
    Col,
    Button,
} from '@bootstrap-styled/v4';
import { createGlobalStyle } from 'styled-components'
import {Sidebar} from './components/Sidebar';

const App = () => {
    const { themeStore } = useStores()
    const GlobalStyle = createGlobalStyle`* {margin:0; padding:0;} html, body, #root, #root > div {height: 100%;} .left-panel { background: #4C5760}`

    return(
        <BootstrapProvider theme={toJS(themeStore.theme)} reset={true}>
            <Container fluid={true} className="p-0 h-100">
                <Row noGutters={true} className="h-100">
                    <Sidebar />
                    <Col md={10}>
                        lal
                    </Col>
                </Row>
            </Container>
            <GlobalStyle />
        </BootstrapProvider>
    )
}

ReactDOM.render(
    <App />,
    document.getElementById('root')
);