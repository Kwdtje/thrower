import React from 'react'
import {ThemeStore} from "../stores/ThemeStore";
import {MenuStore} from "../stores/MenuStore";

export const storesContext = React.createContext({
    themeStore: new ThemeStore(),
    MenuStore: new MenuStore()
})