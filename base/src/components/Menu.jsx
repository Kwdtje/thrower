import React, { useState, useEffect } from "react";
import { observer } from "mobx-react"
import { useStores } from '../hooks/use-stores'
import styled from 'styled-components'
import {Col, Row} from "@bootstrap-styled/v4";

export const Menu = observer((menu) => {
    const { MenuStore } = useStores();

    useEffect(() => {
        MenuStore.getMenu(menu);
    });

    return MenuStore.activeMenu[menu.menu] ? (
        <>lol

        </>
    ) : <> Loading </>
});