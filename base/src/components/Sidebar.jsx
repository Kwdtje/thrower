import React from 'react'
import { observer } from 'mobx-react'
import { useStores } from '../hooks/use-stores'
import styled from 'styled-components'
import {Col, Row} from "@bootstrap-styled/v4";
import {Menu} from './Menu';

export const Sidebar = observer(() => {
    const SideBarCol = ({ className, children }) => (
        <Col md={2} className={className}>
            {children}
        </Col>
    );

    const ThemedSidebar = styled(SideBarCol)`
    ${(props) => `
     background-color: ${props.theme['$brand-inverse']};
     color: ${props.theme['$body-inverse']};
  `};
`;
    return (
        <>
            <ThemedSidebar >
                <Menu menu="base" />
            </ThemedSidebar>
        </>
    )
})