import { observable, action } from 'mobx'

export class ThemeStore {
    @observable
    theme = {
        '$font-family-sans-serif' : '',
        '$brand-primary' : '#39A0ED',
        '$brand-success' : '#5cb85c',
        '$brand-info' : '#5bc0de',
        '$brand-warning' : '#f0ad4e',
        '$brand-danger' : '#d9534f',
        '$brand-inverse' : '#4C5760',
        '$enable-rounded' : true,
        '$enable-shadows' : false,
        '$enable-gradients' : false,
        '$enable-transitions' : true,
        '$enable-hover-media-query' : false,
        '$enable-grid-classes' : true,
        '$enable-print-styles' : true,
        '$body-color' : '#4C5760',
        '$body-inverse' : '#ffffff',
        '$body-bg' : '#ffffff'
    }

    @action
    setTheme(newTheme) {
        this.theme = newTheme
    }
}