import BaseService from './BaseService';

 class MenuService {
     endPoint = 'menu';

     getMenu(menu, callback) {
         let route = [this.endPoint];

         if (menu.menu) {
             route.push(menu.menu);
         }

         BaseService.get(route.join('/'), (response, data) => {
             callback(data);
         })
     }
 }

const menuService = new MenuService();
export default menuService;