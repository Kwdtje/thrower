import axios from 'axios';

class BaseService {
    constructor() {
        this.baseUrl = 'http://localhost:8000/api';
        this.responseType = 'json';
        this.token = '';
        this.headers = {
            //     'Content-Type': 'application/vnd.api+json',
            //     Authorization: `Bearer ${token}`,
        }

        let service = axios.create({
            baseURL: this.baseUrl,
            responseType: this.responseType
        });

        service.interceptors.response.use(this.handleSuccess, this.handleError);
        this.service = service;
    }

    handleSuccess(response) {
        return response;
    }

    handleError = (error) => {
        switch (error.response.status) {
            case 401:
                // this.redirectTo(document, '/')
                break;
            case 404:
                console.log('not found')
                // this.redirectTo(document, '/404')
                break;
            default:
                // this.redirectTo(document, '/500')
                break;
        }
        return Promise.reject(error)
    }

    get(path, callback) {
        return this.service.get(path).then(
            (response) => callback(response.status, response.data)
        );
    }
}

export default new BaseService;

