<?php

namespace Base\Controllers;

use App\Http\Controllers\Controller;
use Base\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function show($id)
    {
        return response()->json(Menu::find($id)->with('items'));
    }
}